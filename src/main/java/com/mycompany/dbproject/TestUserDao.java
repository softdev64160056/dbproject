/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package com.mycompany.dbproject;

import com.mycompany.dbproject.dao.UserDao;
import com.mycompany.dbproject.helper.DatabaseHelper;
import com.mycompany.dbproject.model.User;

/**
 *
 * @author ACER
 */
public class TestUserDao {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        for (User u : userDao.getAll()) {
            System.out.println(u);
        }

        User user1 = userDao.get(3);
        System.out.println(user1);
        //User newUser = new User("user3", "password", 2, "F");
        // User insertedUser = userDao.save(newUser);
        // System.out.println(insertedUser);
        // insertedUser.setGender("M");
        //user1.setGender("F");
        //userDao.update(user1);
        //User updateUser = userDao.get(user1.getId());
        //System.out.println(updateUser);

        //userDao.delete(user1);
        //for(User u: userDao.getAll()) {
        //    System.out.println(u);
        //}
        
        for(User u: userDao.getAll(" user_name like 'u%' ", " user_name asc, user_gender desc ")) {
            System.out.println(u);
        }
        DatabaseHelper.close();
    }

}
