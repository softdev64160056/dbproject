/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dbproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author ACER
 */
public class UpdateDatabase {

    public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:D_coffee.db";
        //connection database
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been establist.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }

        //Update Database
        String sql = "UPDATE category SET cate_name = ? WHERE cate_id = ?;";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, "candy");
            stmt.setInt(2, 6);
            
            int status = stmt.executeUpdate();
            //ResultSet key = pstmt.getGeneratedKeys();
            //key.next();
            //System.out.println(""+key.getInt(1));

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        //Close database
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
